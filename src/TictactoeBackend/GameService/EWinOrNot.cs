﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameService;

internal enum EWinOrNot
{
    Playing = 0,
    XWin = 1,
    OWin = 2,
    Draw = 3
}
