﻿using RabbitMQ.Client.Events;
using System.Text;

namespace GameService
{
    internal class Program
    {
        static async Task Main(string[] args)
        {
            GameManager manager = new GameManager();
            // чтобы приложение не остановилось
            await Task.Delay(Timeout.Infinite);
        }
    }
}
