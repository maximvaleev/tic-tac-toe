﻿namespace GameService;

public enum EGameAction
{
    None = 0,
    P1Button = 1,
    P2Button = 2,
    P1GiveUp = 3,
    P2GiveUp = 4,
    P1Left = 5,
    P2Left = 6,
    CreateGame = 7,
    DeleteGame = 8
}
