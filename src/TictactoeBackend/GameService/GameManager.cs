﻿using RabbitMQ.Client.Events;
using System.Text.Json;

namespace GameService;

internal class GameManager
{
    private readonly MessageConsumer _consumer;
    private readonly MessagePublisher _publisher;
    private readonly Dictionary<int, Game> _games;
    private readonly object _lock = new();

    public GameManager()
    {
        _games = new Dictionary<int, Game>();
        _publisher = new();
        _consumer = new(ReceiveMessage);
    }

    private void ReceiveMessage(object? model, BasicDeliverEventArgs evtArgs)
    {
        _ = Task.Run(() => HandleGameRequest(evtArgs.Body.ToArray()));
    }

    private void HandleGameRequest(byte[] bytes)
    {
        GameRequestDto request = JsonSerializer.Deserialize<GameRequestDto>(new ReadOnlySpan<byte>(bytes))
            ?? throw new Exception("Проблемы с десериализацией");

        EGameAction gameAction = (EGameAction)request.GameActionCode;
        // получить игру
        Game game = null!;
        if (gameAction != EGameAction.CreateGame)
        {
            lock (_lock)
            {
                if (!_games.TryGetValue(request.GameId, out game!))
                {
                    throw new Exception("Нет игры.");
                }
            }
        }
        GameDto? gameDto = null;
        switch (gameAction)
        {
            case EGameAction.P1Button:
                gameDto = game.Player1PressedButton(request.ButtonIndex);
                break;
            case EGameAction.P2Button:
                gameDto = game.Player2PressedButton(request.ButtonIndex);
                break;
            case EGameAction.CreateGame:
                lock (_lock)
                {
                    _games[request.GameId] = new Game(request.GameId);
                    gameDto = _games[request.GameId].ToGameDto();
                }
                break;
            case EGameAction.DeleteGame:
                lock (_lock)
                {
                    _ = _games.Remove(request.GameId);
                }
                break;
            case EGameAction.P1Left:
                gameDto = game.Player1Left();
                break;
            case EGameAction.P2Left:
                gameDto = game.Player2Left();
                break;
            case EGameAction.P1GiveUp:
                gameDto = game.Player1GaveUp();
                break;
            case EGameAction.P2GiveUp:
                gameDto = game.Player2GaveUp();
                break;
            case EGameAction.None:
            default:
                throw new Exception("Неучтенные случаи switch-case");
        }
        // отправить сообщение в рэбита
        if (gameDto != null)
        {
            byte[] bytesToSend = JsonSerializer.SerializeToUtf8Bytes(gameDto);
            _publisher.Publish(bytesToSend);
        }
    }
}
