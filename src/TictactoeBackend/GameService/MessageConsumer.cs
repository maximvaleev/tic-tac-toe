﻿using RabbitMQ.Client.Events;
using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;

namespace GameService;

internal class MessageConsumer
{
    const string _queueName = "RequestQueue";
    private EventingBasicConsumer _consumer;
    private IModel _channel;

    public MessageConsumer(EventHandler<BasicDeliverEventArgs> receivedMessageHandler)
    {
        ConnectionFactory factory = new()
        {
            HostName = "rabbitmq",
            Port = 5672,
            UserName = "guest",
            Password = "guest"
        };
        var connection = factory.CreateConnection();
        _channel = connection.CreateModel();

        _ = _channel.QueueDeclare(
            queue: _queueName,
            exclusive: false,
            durable: true,
            autoDelete: false);

        _consumer = new EventingBasicConsumer(_channel);

        _consumer.Received += receivedMessageHandler;

        _channel.BasicConsume(
            queue: _queueName,
            autoAck: true,
            consumer: _consumer);
    }

    // пример того, как должен выглядеть обработчик полученного сообщения
    private void ReceiveMessage (object? model, BasicDeliverEventArgs evtArgs)
    {
        byte[] messageBytes = evtArgs.Body.ToArray();
        string message = Encoding.UTF8.GetString(messageBytes);
        Console.WriteLine($"Получено сообщение: {message}");
    }
}
