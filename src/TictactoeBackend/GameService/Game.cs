﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameService;

public class Game
{
    public int GameId { get; }
    public EGameState GameState { get; private set;}
    public bool Player1PlaysX { get; }
    public int TurnNumber { get; private set; }
    private readonly Board _board;

    private const int _turnTime = 60_000;
    private readonly Timer _turnTimer;


    public Game(int gameId)
    {
        GameId = gameId;
        _board = new();
        // случайно определяем кто ходит первым
        if (Random.Shared.Next(100) < 50)
        {
            Player1PlaysX = true;
            GameState = EGameState.Player1Turn;
        }
        else
        {
            Player1PlaysX = false;
            GameState = EGameState.Player2Turn;
        }
        TurnNumber = 1;
        _turnTimer = new(TurnTimeout, null, _turnTime, Timeout.Infinite);
    }

    private void TurnTimeout(object? _)
    {
        _turnTimer.Dispose();
        if (GameState == EGameState.Player1Turn) 
        {
            GameState = EGameState.Player1Timeout;
        }
        else if (GameState == EGameState.Player2Turn)
        {
            GameState = EGameState.Player2Timeout;
        }
        else
        {
            throw new Exception("Где то забыл обновить таймер");
        }
    }

    public GameDto Player1PressedButton(int buttonIndex)
    {
        if (GameState == EGameState.Player1Turn)
        {
            if (Player1PlaysX)
            {
                if(_board.TryPutX(buttonIndex))
                {
                    CheckWin();
                }
            }
            else
            {
                if (_board.TryPutO(buttonIndex))
                {
                    CheckWin();
                }
            }
        }

        return ToGameDto();
    }

    private void CheckWin()
    {
        if (GameState != EGameState.Player1Turn && GameState != EGameState.Player2Turn)
        {
            throw new Exception("Проверка победы возможна только если игра еще идет");
        }

        EWinOrNot state = _board.CheckWin();
        switch (state)
        {
            case EWinOrNot.Playing:
                _turnTimer.Change(_turnTime, Timeout.Infinite);
                if (GameState == EGameState.Player1Turn)
                {
                    GameState = EGameState.Player2Turn;
                }
                else
                {
                    GameState = EGameState.Player1Turn;
                }
                TurnNumber++;
                break;
            case EWinOrNot.XWin:
                _turnTimer.Dispose();
                if (Player1PlaysX)
                {
                    GameState = EGameState.Player1Win;
                }
                else
                {
                    GameState = EGameState.Player2Win;
                }
                break;
            case EWinOrNot.OWin:
                _turnTimer.Dispose();
                if (Player1PlaysX)
                {
                    GameState = EGameState.Player2Win;
                }
                else
                {
                    GameState = EGameState.Player1Win;
                }
                break;
            case EWinOrNot.Draw:
                _turnTimer.Dispose();
                GameState = EGameState.Draw;
                break;
        }
    }

    public GameDto Player2PressedButton(int buttonIndex)
    {
        if (GameState == EGameState.Player2Turn)
        {
            if (Player1PlaysX)
            {
                if (_board.TryPutO(buttonIndex))
                {
                    CheckWin();
                }
            }
            else
            {
                if (_board.TryPutX(buttonIndex))
                {
                    CheckWin();
                }
            }
        }

        return ToGameDto();
    }

    public GameDto Player1GaveUp()
    {
        _turnTimer.Dispose();
        GameState = EGameState.Player1GiveUp;
        return ToGameDto();
    }
    public GameDto Player2GaveUp()
    {
        _turnTimer.Dispose();
        GameState = EGameState.Player2GiveUp;
        return ToGameDto();
    }

    public GameDto Player1Left()
    {
        _turnTimer.Dispose();
        GameState = EGameState.Player1Left;
        return ToGameDto();
    }

    public GameDto Player2Left()
    {
        _turnTimer.Dispose();
        GameState = EGameState.Player2Left;
        return ToGameDto();
    }

    public GameDto ToGameDto()
    {
        return new GameDto(
            gameId: GameId,
            gameStateCode: (int)GameState,
            player1PlaysX: Player1PlaysX,
            turnNumber: TurnNumber,
            gameBoard: _board.ToArray()
            );
    }
}
