﻿namespace GameService;

public class GameDto
{
    public int GameId { get; set; }
    public int GameStateCode { get; set; }
    public bool Player1PlaysX { get; set; }
    public int TurnNumber { get; set; }
    public int[] GameBoard { get; set; } = null!;

    public GameDto(int gameId, int gameStateCode, bool player1PlaysX, int turnNumber, int[] gameBoard)
    {
        GameId = gameId;
        GameStateCode = gameStateCode;
        Player1PlaysX = player1PlaysX;
        TurnNumber = turnNumber;
        GameBoard = gameBoard;
    }

    public static GameDto GetFAKE()
    {
        return new GameDto(4, 1, true, 5, new[] { 1, 0, 2, 0, 1, 0, 0, 0, 2 });
    }
}


