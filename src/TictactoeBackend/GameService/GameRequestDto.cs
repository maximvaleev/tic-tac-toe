﻿namespace GameService;

public class GameRequestDto
{
    public int GameId { get; set; }
    public int GameActionCode { get; set; }
    public int ButtonIndex { get; set; }
}
