﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameService;

internal enum ECell
{
    Empty = 0,
    X = 1,
    O = 2,
    WinX = 3,
    WinO = 4
}
