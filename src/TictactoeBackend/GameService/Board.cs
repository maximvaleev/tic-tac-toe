﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameService;

public class Board
{
    private static readonly List<bool[]> _winConditions = new ()
    {
        // ряды
        new bool[] { true, true, true, false, false, false, false, false, false },
        new bool[] { false, false, false, true, true, true, false, false, false },
        new bool[] { false, false, false, false, false, false, true, true, true },
        // столбцы
        new bool[] { true, false, false, true, false, false, true, false, false },
        new bool[] { false, true, false, false, true, false, false, true, false },
        new bool[] { false, false, true, false, false, true, false, false, true },
        // диагонали
        new bool[] { true, false, false, false, true, false, false, false, true },
        new bool[] { false, false, true, false, true, false, true, false, false },
    };
    
    private readonly ECell[] _board;

    public Board()
    {
        _board = new ECell[9];
    }

    public int[] ToArray()
    {
        return new int[] 
        { 
            (int)_board[0], (int)_board[1], (int)_board[2],
            (int)_board[3], (int)_board[4], (int)_board[5],
            (int)_board[6], (int)_board[7], (int)_board[8]
        };
    }

    internal bool TryPutX(int buttonIndex)
    {
        if (_board[buttonIndex] == ECell.Empty) 
        {
            _board[buttonIndex] = ECell.X;
            return true;
        }
        return false;
    }

    internal bool TryPutO(int buttonIndex)
    {
        if (_board[buttonIndex] == ECell.Empty)
        {
            _board[buttonIndex] = ECell.O;
            return true;
        }
        return false;
    }

    internal EWinOrNot CheckWin()
    {
        // Проверка победы
        foreach (bool[] condition in _winConditions)
        {
            int matchCount = 0;
            // проверить крестики
            for (int i = 0; i < 9; i++)
            {
                if (condition[i] && _board[i] == ECell.X)
                {
                    matchCount++;
                    if (matchCount == 3)
                    {
                        // это победа крестиков. Прописать победную комбинацию в поле
                        for (int j = 0; j < 9; j++)
                        {
                            if (condition[j])
                            {
                                _board[j] = ECell.WinX;
                            }
                        }
                        return EWinOrNot.XWin;
                    }
                }
            }
            // проверить нолики
            matchCount = 0;
            for (int i = 0; i < 9; i++)
            {
                if (condition[i] && _board[i] == ECell.O)
                {
                    matchCount++;
                    if (matchCount == 3)
                    {
                        // это победа ноликов. Прописать победную комбинацию в поле
                        for (int j = 0; j < 9; j++)
                        {
                            if (condition[j])
                            {
                                _board[j] = ECell.WinO;
                            }
                        }
                        return EWinOrNot.OWin;
                    }
                }
            }
        }
        // Проверка ничьей
        int filledCells = 0;
        for (int i = 0; i < 9; i++)
        {
            if (_board[i] != ECell.Empty)
            {
                filledCells++;
            }
            else
            {
                break;
            }
        }
        if (filledCells == 9)
        {
            return EWinOrNot.Draw;
        }

        return EWinOrNot.Playing;
    }
}
