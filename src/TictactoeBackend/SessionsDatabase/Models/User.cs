﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace SessionsDatabase.Models;

public class User
{
    public int Id { get; set; }
    public string UserName { get; set; } = null!;
    public string Password { get; set; } = null!;
    public int WinsCount { get; set; }
    public int LossesCount { get; set; }
    public int DrawsCount { get; set; }
}
