﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using SessionsDatabase.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace SessionsDatabase.Contexts;

public class UsersContext : DbContext
{
    public UsersContext(DbContextOptions<UsersContext> options) : base(options)
    {
        Database.EnsureCreated();
    }

    public DbSet<User> Users { get; set; }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<User>().HasData(
                new User { Id = 1, UserName = "admin", Password = "admin", WinsCount = 4, LossesCount = 5, DrawsCount = 6 },
                new User { Id = 2, UserName = "max", Password = "max", WinsCount = 3, LossesCount = 4, DrawsCount = 5 }
        );
    }
}
