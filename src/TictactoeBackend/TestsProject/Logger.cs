﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestsProject;

public class Logger : IDisposable
{
    private readonly string _fileName;
    private readonly StreamWriter _sw;
    private int _messageNumber;

    public Logger(string logName)
    {
        _fileName = Path.Combine("Logs", logName + ".txt");
        
        if (File.Exists(_fileName))
        {
            File.Delete(_fileName);
        }
        _sw = File.CreateText(_fileName);

        Log("Program started");
    }

    public void Log(string message)
    {
        _messageNumber++;
        _sw.WriteLine($"------------------{_messageNumber}. {DateTime.Now.ToLongTimeString()} --------------------");
        _sw.WriteLine(message);
        _sw.WriteLine("--------------------------------------------------");
    }

    public void LogGameBoard(int[] board)
    {
        _messageNumber++;
        _sw.WriteLine($"------------------{_messageNumber}. {DateTime.Now.ToLongTimeString()} -----------");
        _sw.WriteLine("Состояние доски:");
        _sw.WriteLine("╔═══╤═══╤═══╗");
        _sw.WriteLine($"║{GetBoardSymbol(board[0])}│{GetBoardSymbol(board[1])}│{GetBoardSymbol(board[2])}║");
        _sw.WriteLine("╟───┼───┼───╢");
        _sw.WriteLine($"║{GetBoardSymbol(board[3])}│{GetBoardSymbol(board[4])}│{GetBoardSymbol(board[5])}║");
        _sw.WriteLine("╟───┼───┼───╢");
        _sw.WriteLine($"║{GetBoardSymbol(board[6])}│{GetBoardSymbol(board[7])}│{GetBoardSymbol(board[8])}║");
        _sw.WriteLine("╚═══╧═══╧═══╝");
        _sw.WriteLine("--------------------------------------------------");
    }

    private string GetBoardSymbol(int stateCode)
    {
        /*Empty = 0,
        X = 1,
        O = 2,
        WinX = 3,
        WinO = 4*/
        return stateCode switch
        {
            0 => "   ",
            1 => " x ",
            2 => " o ",
            3 => "-X-",
            4 => "-O-",
            _ => throw new Exception("Что то не так с перечислением состояний клетки поля."),
        };
    }

    public void Dispose()
    {
        _sw.Dispose();
    }
}
