﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Net.Http.Json;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace TestsProject;

public sealed class Requests : IDisposable
{
    private readonly HttpClient _client;
    private const string _urlBase = "http://host.docker.internal:8080/api/";
    public string? UserToken { get; set; }
    public int UserId { get; set; } = -1;
    public UserDto? UserData { get; set; }
    public SessionDto? SessionData { get; set; }
    public GameDto? GameData { get; set; }

    public Requests()
    {
        _client = new HttpClient();
        // добавить дефолтные заголовки?
    }

    public async Task<UserIdAndToken> Post_AuthorizeUserAsync(string userName, string password)
    {
        HttpRequestMessage request = new(HttpMethod.Post, _urlBase + "Users")
        {
            Content = new StringContent("{\"userName\": \"" + userName + "\", \"password\": \"" + password + "\"}", Encoding.UTF8, "application/json")
        };

        HttpResponseMessage response = await _client.SendAsync(request);
        var userData = await AnalyzeResponseAsync<UserIdAndToken>(response);

        // Сразу сохраним всю информацию к себе, чтобы не передавать ее аргументами потом
        UserToken = userData.Token;
        UserId = userData.UserId;
        // Пропишем дефолтный заголовок авторизации с токеном
        _ = _client.DefaultRequestHeaders.Remove("Authorization");
        _client.DefaultRequestHeaders.Add("Authorization", "Bearer " + UserToken);
        UserData = null;
        SessionData = null;

        return userData;
    }

    // Если вернулся успех, то десериализуем, если ошибка, то бросаем соответствующее исключение
    private async Task<T> AnalyzeResponseAsync<T>(HttpResponseMessage response)
    {
        if (response.IsSuccessStatusCode)
        {
            T result = await response.Content.ReadFromJsonAsync<T>() ?? throw new Exception("Вернулся неверный формат.");
            return result;
        }
        else
        {
            await ThrowBadStatusCodeExceptionAsync(response);
        }
        return default!; // сюда мы никогда не попадем
    }

    // Если вернулся код ошибки, то бросаем соответствующее исключение
    private async Task ThrowBadStatusCodeExceptionAsync(HttpResponseMessage response)
    {
        string code = (int)response.StatusCode + " - ";
        throw response.StatusCode switch
        {
            System.Net.HttpStatusCode.BadRequest => new ArgumentException(code + await response.Content.ReadAsStringAsync()),
            System.Net.HttpStatusCode.Unauthorized => new UnauthorizedAccessException(code + await response.Content.ReadAsStringAsync()),
            System.Net.HttpStatusCode.NotFound => new KeyNotFoundException(code + await response.Content.ReadAsStringAsync()),
            _ => new Exception(code + await response.Content.ReadAsStringAsync()),
        };
    }

    public async Task<UserDto> Get_UserDataByIdAsync(int userId)
    {
        HttpResponseMessage response = await _client.GetAsync(_urlBase + "Users/" + userId);
        var userData = await AnalyzeResponseAsync<UserDto>(response);

        // если мы спрашивали про себя, то запишем информацию про себя
        if (userData.Id == UserId)
        {
            UserData = userData;
        }

        return userData;
    }

    public async Task<UserDto> Get_MyUserDataAsync() => await Get_UserDataByIdAsync(UserId);

    public async Task<SessionDto> Post_CreateSessionForCurrentUserAsync()
    {
        // пользователь определяется на бэке по токену
        HttpResponseMessage response = await _client.PostAsync(_urlBase + "Sessions", content: null);
        var sessionData = await AnalyzeResponseAsync<SessionDto>(response);

        SessionData = sessionData;

        return sessionData;
    }

    public async Task<IEnumerable<SessionIdNameDto>> Get_CreatedSessionsCollectionAsync()
    {
        HttpResponseMessage response = await _client.GetAsync(_urlBase + "Sessions");
        var sessions = await AnalyzeResponseAsync<IEnumerable<SessionIdNameDto>>(response);

        return sessions;
    }

    public async Task<SessionDto> Put_UserAsPlayer2ToSessionAsync(int sessionId)
    {
        HttpResponseMessage response = await _client.PutAsync(_urlBase + "Sessions/" + sessionId, content: null);
        var sessionData = await AnalyzeResponseAsync<SessionDto>(response);
        SessionData = sessionData;
        
        return sessionData;
    }

    public async Task Delete_CurrentUserFromSessionAsync(int sessionId)
    {
        HttpResponseMessage response = await _client.DeleteAsync(_urlBase + "Sessions/" + sessionId);
        if (!response.IsSuccessStatusCode)
        {
            await ThrowBadStatusCodeExceptionAsync(response);
        }
        // что то нужно сделать с MySessionData?
    }

    public async Task Delete_MeFromMySessionAsync() => await Delete_CurrentUserFromSessionAsync(UserId);

    public async Task<SessionDto> Get_SessionInfoAsync(int sessionId)
    {
        HttpResponseMessage response = await _client.GetAsync(_urlBase + "Sessions/" + sessionId);
        var sessionData = await AnalyzeResponseAsync<SessionDto>(response);
        SessionData = sessionData;

        return sessionData;
    }

    public async Task<SessionDto> Get_MySessionInfoAsync() => await Get_SessionInfoAsync(UserId);

    public async Task<GameDto> Get_GameInfoAsync(int sessionId)
    {
        HttpResponseMessage response = await _client.GetAsync(_urlBase + "Sessions/" + sessionId + "/game");
        var gameData = await AnalyzeResponseAsync<GameDto>(response);
        GameData = gameData;

        return gameData;
    }

    public async Task<GameDto> Get_MyGameInfoAsync() => await Get_GameInfoAsync(UserId);

    public async Task Put_GameButtonPressedAsync(int sessionId, int cellId)
    {
        HttpResponseMessage response = await _client.PutAsync(_urlBase + "Sessions/" + sessionId + "/game?cell=" + cellId, null);
        if (!response.IsSuccessStatusCode)
        {
            await ThrowBadStatusCodeExceptionAsync(response);
        }
    }

    public async Task Put_GiveupButtonPressedAsync(int sessionId)
    {
        HttpResponseMessage response = await _client.PutAsync(_urlBase + "Sessions/" + sessionId + "/game/giveup", null);
        if (!response.IsSuccessStatusCode)
        {
            await ThrowBadStatusCodeExceptionAsync(response);
        }
    }

    public async Task Put_PlayAgainButtonPressedAsync(int sessionId)
    {
        HttpResponseMessage response = await _client.PutAsync(_urlBase + "Sessions/" + sessionId + "/game/playagain", null);
        if (!response.IsSuccessStatusCode)
        {
            await ThrowBadStatusCodeExceptionAsync(response);
        }
    }

    public void Dispose() => _client.Dispose();
}
