﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestsProject.TestCases;

internal class TestPlay : ITestScenario
{
    /* Пользователи логинятся
     * Пользователь 1 создает сессию
     * Польз.2 подключается в сессию к первому
     * Играют партию.
     * Соглашаются играть еще раз
     * Игрок 1 сдается
     */

    private const string _logFileName = "TestPlay";
    private readonly int[] _gameMoves = [4, 1, 6, 2, 0, 3, 8];

    public async Task Run()
    {
        try
        {
            using RequestHelper request = new(_logFileName);
            
            // Пользователь 1 логинится
            UserIdAndToken user1IdAndToken = await request.LoginUser1("admin", "admin");
            // Пользователь 1 получает о себе инфо
            UserDto? user1Data = await request.GetUser1Data();
            // Запрос списка активных сессий
            IEnumerable<SessionIdNameDto>? sessionsCollection = await request.GetSessionsCollection();
            // Пользователь 1 создает сессию
            SessionDto session = await request.CreateSession();
            // Еще раз Запрос списка активных сессий
            sessionsCollection = await request.GetSessionsCollection();
            // Польз.2 логинится
            UserIdAndToken user2IdAndToken = await request.LoginUser2("test", "test");
            // Пользователь 2 получает о себе инфо
            UserDto? user2Data = await request.GetUser2Data();
            // Польз. 2 подключается к сессии польз. 1
            session = await request.AssignPlayer2ToSession();
            // Игра создается автоматически. Ждем и запрашиваем
            await Task.Delay(2000);
            session = await request.GetSessionInfo() ?? throw new Exception("Инфо по сессии не удалось получить");
            // Запросить статус игры
            GameDto game = await request.GetGameInfo() ?? throw new Exception("Инфо по игре не удалось получить");
            await PlayGame(game, request);
            session = await request.GetSessionInfo() ?? throw new Exception("Инфо по сессии не удалось получить");
            await request.Player1PressPlayAgain();
            await request.Player2PressPlayAgain();
            await Task.Delay(2000);
            session = await request.GetSessionInfo() ?? throw new Exception("Инфо по сессии не удалось получить");
            game = await request.GetGameInfo() ?? throw new Exception("Инфо по игре не удалось получить");
            await request.Player1PressGiveUp();
            await Task.Delay(2000);
            session = await request.GetSessionInfo() ?? throw new Exception("Инфо по сессии не удалось получить");
            game = await request.GetGameInfo() ?? throw new Exception("Инфо по игре не удалось получить");
            user1Data = await request.GetUser1Data();
            user2Data = await request.GetUser2Data();
        }
        catch
        {
            // ничего не делаем
        }
    }

    private async Task PlayGame(GameDto game, RequestHelper request)
    {
        int i = 0;
        if (game.GameStateCode == 1)
        {
            // ходит первый игрок крестиками
            await request.Player1PressGameButton(_gameMoves[0]);
            await Task.Delay(2000);
            game = await request.GetGameInfo() ?? throw new Exception("Инфо по игре не удалось получить");
            i++;
        }
        // иначе считаем что ходит второй игрок крестиками
        while(true)
        {
            await request.Player2PressGameButton(_gameMoves[i]);
            await Task.Delay(2000);
            game = await request.GetGameInfo() ?? throw new Exception("Инфо по игре не удалось получить");
            i++;
            if (i == _gameMoves.Length)
            {
                break;
            }
            await request.Player1PressGameButton(_gameMoves[i]);
            await Task.Delay(2000);
            game = await request.GetGameInfo() ?? throw new Exception("Инфо по игре не удалось получить");
            i++;
            if (i == _gameMoves.Length)
            {
                break;
            }
        }
    }
}
