﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestsProject.TestCases;

internal class TestSessionConnection : ITestScenario
{
    /* Пользователь 1 логинится
     * Пользователь 1 создает сессию
     * Польз.2 логинится
     * Польз.2 подключается в сессию к первому
     * Польз.1 уходит со страницы
     */

    private const string _logFileName = "TestSessionConnection";

    public async Task Run()
    {
        try
        {
            using RequestHelper request = new(_logFileName);
            
            // Пользователь 1 логинится
            UserIdAndToken user1IdAndToken = await request.LoginUser1("admin", "admin");
            // Пользователь 1 получает о себе инфо
            UserDto? user1Data = await request.GetUser1Data();
            // Запрос списка активных сессий
            IEnumerable<SessionIdNameDto>? sessionsCollection = await request.GetSessionsCollection();
            // Пользователь 1 создает сессию
            SessionDto session = await request.CreateSession();
            // Еще раз Запрос списка активных сессий
            sessionsCollection = await request.GetSessionsCollection();
            // Польз.2 логинится
            UserIdAndToken user2IdAndToken = await request.LoginUser2("test", "test");
            // Пользователь 2 получает о себе инфо
            UserDto? user2Data = await request.GetUser2Data();
            // Польз. 2 подключается к сессии польз. 1
            session = await request.AssignPlayer2ToSession();
            // Игра создается автоматически. Ждем и запрашиваем
            await Task.Delay(1000);
            session = await request.GetSessionInfo() ?? throw new Exception("Инфо по сессии не удалось получить");
            // Игрок 1 уходит со страницы
            await request.DeleteUser1FromSession();
            // Еще раз запросить статус сессии
            await Task.Delay(1000);
            session = await request.GetSessionInfo() ?? throw new Exception("Инфо по сессии не удалось получить");
        }
        catch
        {
            // ничего не делаем
        }
    }
}
