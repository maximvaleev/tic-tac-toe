﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestsProject;

internal class RequestHelper : IDisposable
{
    public Requests User1Requests { get; }
    public Requests User2Requests { get; }
    public Logger Logger { get; }
    public Dictionary<int, string> SessionStates { get; }
    public Dictionary<int, string> GameStates { get; }

    public RequestHelper(string testName)
    {
        Logger = new Logger(testName);
        User1Requests = new Requests();
        User2Requests = new Requests();
        SessionStates = new Dictionary<int, string>() {
            {0, "Создана и ожидает второго игрока" },
            {1, "Идет игра" },
            {2, "Игра завершена" },
            {3, "Деактивирована и будет удалена" }
        };
        GameStates = new Dictionary<int, string>() {
            {0, "ОШИБКА" },
            {1, "Ход 1-го игрока" },
            {2, "Ход 2-го игрока" },
            {3, "Игрок 1 победил" },
            {4, "Игрок 2 победил" },
            {5, "Игра закончилась ничьей" },
            {6, "Игрок 1 покинул игру" },
            {7, "Игрок 2 покинул игру" },
            {8, "Игрок 1 сдался" },
            {9, "Игрок 2 сдался" },
            {10, "Игрок 1 проиграл по таймауту" },
            {11, "Игрок 2 проиграл по таймауту" }
        };
        Logger.Log($"Запуск тестового сценария '{testName}'");
    }

    public async Task<UserIdAndToken> LoginUser1(string userName, string password)
    {
        try
        {
            UserIdAndToken result = await User1Requests.Post_AuthorizeUserAsync(userName, password);
            Logger.Log($"Пользователь '{userName}' авторизовался. ID: {result.UserId}, Token: {result.Token}");
            return result;
        }
        catch (Exception exc)
        {
            Logger.Log($"Ошибка при авторизации пользователя: {exc.Message}");
            Logger.Log("Не удалось завершить сценарий.");
            throw new Exception("Не удалось завершить сценарий: " + exc.Message);
        }
    }

    public async Task<UserIdAndToken> LoginUser2(string userName, string password)
    {
        try
        {
            UserIdAndToken result = await User2Requests.Post_AuthorizeUserAsync(userName, password);
            Logger.Log($"Пользователь '{userName}' авторизовался. ID: {result.UserId}, Token: {result.Token}");
            return result;
        }
        catch (Exception exc)
        {
            Logger.Log($"Ошибка при авторизации пользователя: {exc.Message}");
            Logger.Log("Не удалось завершить сценарий.");
            throw new Exception("Не удалось завершить сценарий: " + exc.Message);
        }
    }

    // сессия создается для первого пользователя!
    public async Task<SessionDto> CreateSession()
    {
        try
        {
            SessionDto session = await User1Requests.Post_CreateSessionForCurrentUserAsync();
            Logger.Log($"Пользователь '{User1Requests.UserData?.UserName}' создал сессию. Состояние сессии: {SessionStates[session.ESessionState]}");
            return session;
        }
        catch (Exception exc)
        {
            Logger.Log($"Ошибка при создании сессии: {exc.Message}");
            Logger.Log("Не удалось завершить сценарий.");
            throw new Exception("Не удалось завершить сценарий: " + exc.Message);
        }
    }

    public async Task<UserDto?> GetUser1Data()
    {
        try
        {
            UserDto userData = await User1Requests.Get_MyUserDataAsync();
            Logger.Log($"Инфо о пользователе. Имя: {userData.UserName}, ID: {userData.Id}, Побед: {userData.WinsCount}, Поражений: {userData.LossesCount}, Ничьих: {userData.DrawsCount}.");
            return userData;
        }
        catch (Exception exc)
        {
            Logger.Log($"Ошибка при получении информации о пользователе: {exc.Message}");
            return null;
        }
    }

    public async Task<UserDto?> GetUser2Data()
    {
        try
        {
            UserDto userData = await User2Requests.Get_MyUserDataAsync();
            Logger.Log($"Инфо о пользователе. Имя: {userData.UserName}, ID: {userData.Id}, Побед: {userData.WinsCount}, Поражений: {userData.LossesCount}, Ничьих: {userData.DrawsCount}.");
            return userData;
        }
        catch (Exception exc)
        {
            Logger.Log($"Ошибка при получении информации о пользователе: {exc.Message}");
            return null;
        }
    }

    public async Task<IEnumerable<SessionIdNameDto>?> GetSessionsCollection()
    {
        try
        {
            IEnumerable<SessionIdNameDto> sessions = await User1Requests.Get_CreatedSessionsCollectionAsync();
            Logger.Log($"Список активных сессий: {string.Join(", ", sessions.Select(x => x.Name))}");
            return sessions;
        }
        catch (Exception exc)
        {
            Logger.Log($"Ошибка при получении активных сессий: {exc.Message}");
            return null;
        }
    }

    public async Task<SessionDto> AssignPlayer2ToSession()
    {
        try
        {
            SessionDto session = await User2Requests.Put_UserAsPlayer2ToSessionAsync(User1Requests.UserId);
            Logger.Log($"Пользователь '{User2Requests.UserData?.UserName}' добавлен в сессию к пользователю '{User1Requests.UserData?.UserName}'");
            return session;
        }
        catch (Exception exc)
        {
            Logger.Log($"Ошибка при добавлении пользователя в сессию: {exc.Message}");
            Logger.Log("Не удалось завершить сценарий.");
            throw new Exception("Не удалось завершить сценарий: " + exc.Message);
        }
    }

    public async Task DeleteUser1FromSession()
    {
        try
        {
            await User1Requests.Delete_CurrentUserFromSessionAsync(User1Requests.UserId);
            Logger.Log($"Пользователь '{User1Requests.UserData?.UserName}' удален из своей сессии.");
        }
        catch (Exception exc)
        {
            Logger.Log($"Ошибка при удалении пользователя из сессии: {exc.Message}");
        }
    }

    public async Task DeleteUser2FromSession()
    {
        try
        {
            await User2Requests.Delete_CurrentUserFromSessionAsync(User1Requests.UserId);
            Logger.Log($"Пользователь '{User2Requests.UserData?.UserName}' удален из сессии пользователя '{User1Requests.UserData?.UserName}'.");
        }
        catch (Exception exc)
        {
            Logger.Log($"Ошибка при удалении пользователя из сессии: {exc.Message}");
        }
    }

    public async Task<SessionDto?> GetSessionInfo()
    {
        try
        {
            SessionDto sessionData = await User1Requests.Get_MySessionInfoAsync();
            Logger.Log($"Инфо о сессии. Player 1 ID: {sessionData.Player1Id}, Player 2 ID: {sessionData.Player2Id}, Состояние: {SessionStates[sessionData.ESessionState]}.");
            return sessionData;
        }
        catch (Exception exc)
        {
            Logger.Log($"Ошибка при получении информации о сессии: {exc.Message}");
            return null;
        }
    }

    public async Task<GameDto?> GetGameInfo()
    {
        try
        {
            GameDto gameData = await User1Requests.Get_GameInfoAsync(User1Requests.UserId);
            Logger.Log($"Инфо об игре. ID: {gameData.GameId}, " +
                $"Состояние: {GameStates[gameData.GameStateCode]}, " +
                $"Игрок 1 играет крестиками: {gameData.Player1PlaysX}, " +
                $"Номер хода: {gameData.TurnNumber}");
            Logger.LogGameBoard(gameData.GameBoard);
            return gameData;
        }
        catch (Exception exc)
        {
            Logger.Log($"Ошибка при получении информации об игре: {exc.Message}");
            return null;
        }
    }

    public async Task Player1PressGameButton(int cellId)
    {
        try
        {
            await User1Requests.Put_GameButtonPressedAsync(User1Requests.UserId, cellId);
            Logger.Log($"Пользователь '{User1Requests.UserData?.UserName}' нажал игровую кнопку {cellId}.");
        }
        catch (Exception exc)
        {
            Logger.Log($"Ошибка при попытке нажатия кнопки: {exc.Message}");
            Logger.Log("Не удалось завершить сценарий.");
            throw new Exception("Не удалось завершить сценарий: " + exc.Message);
        }
    }

    public async Task Player2PressGameButton(int cellId)
    {
        try
        {
            await User2Requests.Put_GameButtonPressedAsync(User1Requests.UserId, cellId);
            Logger.Log($"Пользователь '{User2Requests.UserData?.UserName}' нажал игровую кнопку {cellId}.");
        }
        catch (Exception exc)
        {
            Logger.Log($"Ошибка при попытке нажатия кнопки: {exc.Message}");
            Logger.Log("Не удалось завершить сценарий.");
            throw new Exception("Не удалось завершить сценарий: " + exc.Message);
        }
    }

    public async Task Player1PressGiveUp()
    {
        try
        {
            await User1Requests.Put_GiveupButtonPressedAsync(User1Requests.UserId);
            Logger.Log($"Пользователь '{User1Requests.UserData?.UserName}' нажал кнопку 'Сдаться'.");
        }
        catch (Exception exc)
        {
            Logger.Log($"Ошибка при попытке нажатия кнопки: {exc.Message}");
            Logger.Log("Не удалось завершить сценарий.");
            throw new Exception("Не удалось завершить сценарий: " + exc.Message);
        }
    }

    public async Task Player2PressGiveUp()
    {
        try
        {
            await User2Requests.Put_GiveupButtonPressedAsync(User1Requests.UserId);
            Logger.Log($"Пользователь '{User2Requests.UserData?.UserName}' нажал кнопку 'Сдаться'.");
        }
        catch (Exception exc)
        {
            Logger.Log($"Ошибка при попытке нажатия кнопки: {exc.Message}");
            Logger.Log("Не удалось завершить сценарий.");
            throw new Exception("Не удалось завершить сценарий: " + exc.Message);
        }
    }

    public async Task Player1PressPlayAgain()
    {
        try
        {
            await User1Requests.Put_PlayAgainButtonPressedAsync(User1Requests.UserId);
            Logger.Log($"Пользователь '{User1Requests.UserData?.UserName}' нажал кнопку 'Сыграть еще раз'.");
        }
        catch (Exception exc)
        {
            Logger.Log($"Ошибка при попытке нажатия кнопки: {exc.Message}");
            Logger.Log("Не удалось завершить сценарий.");
            throw new Exception("Не удалось завершить сценарий: " + exc.Message);
        }
    }

    public async Task Player2PressPlayAgain()
    {
        try
        {
            await User2Requests.Put_PlayAgainButtonPressedAsync(User1Requests.UserId);
            Logger.Log($"Пользователь '{User2Requests.UserData?.UserName}' нажал кнопку 'Сыграть еще раз'.");
        }
        catch (Exception exc)
        {
            Logger.Log($"Ошибка при попытке нажатия кнопки: {exc.Message}");
            Logger.Log("Не удалось завершить сценарий.");
            throw new Exception("Не удалось завершить сценарий: " + exc.Message);
        }
    }

    public void Dispose()
    {
        User1Requests.Dispose();
        User2Requests.Dispose();
        Logger.Dispose();
    }
}
