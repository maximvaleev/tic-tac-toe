﻿namespace TestsProject;

public class SessionDto
{
    public int Player1Id { get; set; }
    public int? Player2Id { get; set; }
    public int ESessionState { get; set; }
}
