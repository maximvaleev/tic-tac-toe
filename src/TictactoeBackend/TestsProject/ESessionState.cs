﻿namespace TestsProject;

public enum ESessionState
{
    Created = 0,
    Playing = 1,
    Results = 2,
    Deactivated = 3
}
