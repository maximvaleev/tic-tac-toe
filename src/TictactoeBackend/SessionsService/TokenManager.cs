﻿using SessionsDatabase.Models;
using System;
using System.Threading;

namespace SessionsService
{
    public class TokenManager : ITokenManager
    {
        const int _tokenTimeout = 600_000;
        const string _randomChars = "abcdefghijklmnopqrstuvwxyz0123456789";
        const int _tokenLength = 8;

        private readonly Dictionary<string, User> _tokensAndUsers;
        private readonly Dictionary<string, Timer> _tokensAndTimers;
        private readonly Random _random;

        public TokenManager()
        {
            _tokensAndUsers = new();
            _tokensAndTimers = new();
            _random = new();
        }

        public string CreateToken(User user)
        {
            string token = GenerateToken();

            _tokensAndUsers.Add(token, user);
            Timer timer = new(RemoveToken, token, _tokenTimeout, Timeout.Infinite);
            _tokensAndTimers.Add(token, timer);

            return token;
        }

        private void RemoveToken(object? tokenObj)
        {
            RemoveToken(tokenObj as string);
        }

        public void RemoveToken(string? token)
        {
            if (token == null) { return; }
            try
            {
                _tokensAndUsers.Remove(token);
                _tokensAndTimers[token].Dispose();
                _tokensAndTimers.Remove(token);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Ошибка при удалении токена: " + ex.Message);
            }
        }

        public User? GetUser(string token)
        {
            if (_tokensAndUsers.TryGetValue(token, out User? user))
            {
                _tokensAndTimers[token].Change(_tokenTimeout, Timeout.Infinite);
            }
            return user;
        }

        public bool IsTokenExists(string token)
        {
            if (_tokensAndUsers.TryGetValue(token, out _))
            {
                _tokensAndTimers[token].Change(_tokenTimeout, Timeout.Infinite);
                return true;
            }
            return false;
        }

        private string GenerateToken()
        {
            var tokenArr = new char[_tokenLength];
            for (int i = 0; i < _tokenLength; i++)
            {
                tokenArr[i] = _randomChars[_random.Next(_randomChars.Length)];
            }
            string token = new(tokenArr);
            // если новый токен совпал с существующим, то рекурсивно нагенерить другой
            if (_tokensAndUsers.TryGetValue(token, out _))
            {
                token = GenerateToken();
            }
            return token;
        }

        // Проверяет токен вида "Bearer a1b2c3d4" на формат и наличие токена среди активных.
        // Если проходит то ничего не возвращает, если не проходит, то бросает UnauthorizedAccessException
        public string ValidateAndCleanseToken(string userToken)
        {
            if (!string.IsNullOrWhiteSpace(userToken) && userToken.StartsWith("Bearer "))
            {
                userToken = userToken.Substring("Bearer ".Length).Trim();
                if (!IsTokenExists(userToken))
                {
                    throw new UnauthorizedAccessException("Токен пользователя неверный или устарел.");
                }
            }
            else
            {
                throw new UnauthorizedAccessException("Необходим токен пользователя.");
            }
            return userToken;
        }

        public void AddTestUser(string token, User? user)
        {
            if (user != null && !_tokensAndUsers.ContainsKey(token)) 
            {
                _tokensAndUsers[token] = user;
                Timer timer = new(RemoveToken, token, 9_999_999, Timeout.Infinite);
                _tokensAndTimers[token] = timer;
            }
        }
    }
}
