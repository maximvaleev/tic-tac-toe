﻿using GameService;
using RabbitMQ.Client.Events;
using SessionsDatabase.Contexts;
using SessionsDatabase.Models;
using SessionsService.Models;
using System.Security.Policy;
using System.Text.Json;

namespace SessionsService;

public class SessionManager : ISessionManager
{
    // key - UserId
    private readonly Dictionary<int, Session> _sessions;
    private readonly MessageConsumer _consumer;
    private readonly MessagePublisher _publisher;

    public SessionManager()
    {
        _sessions = new();
        _publisher = new();
        _consumer = new(ReceiveMessage);
    }

    private void ReceiveMessage(object? model, BasicDeliverEventArgs evtArgs)
    {
        GameDto response = JsonSerializer.Deserialize<GameDto>(new ReadOnlySpan<byte>(evtArgs.Body.ToArray()))
            ?? throw new Exception("Проблемы с десериализацией");
        if (!_sessions.TryGetValue(response.GameId, out Session? session))
        {
            // по идее такого быть не должно, но возможно из за задержек в сообщениях, поэтому не кидаю исключение
            return;
        }
        session.GameDto = response;
        // если сейчас состояние "В игре", то нужно проанализировать состояние игры и возможно изменить состояние сессии.
        session.OnGameStateChanged();
    }

    public Session? GetById(int sessionId)
    {
        _sessions.TryGetValue(sessionId, out Session? session);
        return session;
    }

    public Session GetOrCreate(User user)
    {
        if (!_sessions.TryGetValue(user.Id, out Session? session))
        {
            session = new Session(user, this);
            _sessions.Add(user.Id, session);
        }
        return session;
    }

    public IEnumerable<SessionIdNameDto> GetCreatedSessionsList()
    {
        return _sessions
            .Where(kvp => kvp.Value.GetStateEnum() == Enums.ESessionState.Created)
            .Select(kvp => new SessionIdNameDto(kvp.Value));
    }

    public void DeleteUserFromSession(int sessionId, User user)
    {
        if (!_sessions.TryGetValue(sessionId, out Session? session))
        {
            throw new KeyNotFoundException("Указанной сессии не существует");
        }

        if (session.Player1.Id == user.Id)
        {
            session.RemovePlayer1();
        }
        else if (session.Player2 is not null && session.Player2.Id == user.Id)
        {
            session.RemovePlayer2();
        }
        else
        {
            throw new ArgumentException("Текущий пользователь не принадлежит указанной сессии.");
        }
    }

    public void RemoveSession(int sessionId)
    {
        _ = _sessions.Remove(sessionId);
    }

    public void PublishRequest(GameRequestDto request)
    {
        // отправить сообщение в рэбита
        byte[] bytesToSend = JsonSerializer.SerializeToUtf8Bytes(request);
        _publisher.Publish(bytesToSend);
    }
}
