﻿namespace SessionsService;

public class PlayersStatisticsChanges
{
    public int Player1WinsCount { get; set; }
    public int Player2WinsCount { get; set; }
    public int Player1DrawsCount { get; set; }
    public int Player2DrawsCount { get; set; }
    public int Player1LossesCount { get; set; }
    public int Player2LossesCount { get; set; }
}
