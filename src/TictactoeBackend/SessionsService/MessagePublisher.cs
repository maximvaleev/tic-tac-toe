﻿using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameService;

internal class MessagePublisher
{
    const string _queueName = "RequestQueue";
    private IModel _channel;

    public MessagePublisher()
    {
        ConnectionFactory factory = new()
        {
            HostName = "rabbitmq",
            Port = 5672,
            UserName = "guest",
            Password = "guest"
        };
        var connection = factory.CreateConnection();
        _channel = connection.CreateModel();

        _ = _channel.QueueDeclare(
            queue: _queueName,
            exclusive: false,
            durable: true,
            autoDelete: false);
    }

    public void Publish(string message)
    {
        byte[] messageBytes = Encoding.UTF8.GetBytes(message);
        Publish(messageBytes);
    }

    public void Publish(byte[] messageBytes)
    {
        _channel.BasicPublish(
            exchange: "",
            routingKey: _queueName,
            basicProperties: null,
            body: messageBytes);
    }
}
