﻿using SessionsDatabase.Models;
using SessionsService.Models;

namespace SessionsService;

public interface ISessionManager
{
    Session? GetById(int sessionId);
    Session GetOrCreate(User user);
    IEnumerable<SessionIdNameDto> GetCreatedSessionsList();
    void DeleteUserFromSession(int sessionId, User user);
    void RemoveSession(int sessionId);
    void PublishRequest(GameRequestDto request);
}
