﻿namespace SessionsService.Models;

public class SessionDto
{
    public int Player1Id { get; set; }
    public int? Player2Id { get; set; }
    public int ESessionState { get; set; }

    public SessionDto(Session session)
    {
        Player1Id = session.Player1.Id;
        Player2Id = session.Player2?.Id;
        ESessionState = (int)session.GetStateEnum();
    }
}
