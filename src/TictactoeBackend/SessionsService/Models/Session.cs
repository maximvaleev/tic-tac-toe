﻿using GameService;
using Mono.TextTemplating;
using SessionsDatabase.Models;
using SessionsService.Enums;
using SessionsService.SessionState;

namespace SessionsService.Models;

public class Session : ISessionState
{
    public User Player1 { get; set; }
    public User? Player2 { get; set; }
    public GameDto? GameDto { get; set; }
    public ISessionState State { get; private set; }
    private readonly ISessionManager _sessionManager;

    public Session(User player1, ISessionManager sessionManager)
    {
        Player1 = player1;
        State = new SessionCreatedState(this);
        _sessionManager = sessionManager;
    }

    public void AddPlayer2(User player2) => State.AddPlayer2(player2);

    public void RemovePlayer1() => State.RemovePlayer1();
    public void RemovePlayer2() => State.RemovePlayer2();

    public void Player1GameButtonClicked(int buttonId) => State.Player1GameButtonClicked(buttonId);
    public void Player2GameButtonClicked(int buttonId) => State.Player2GameButtonClicked(buttonId);

    public void Player1GiveUpClicked() => State.Player1GiveUpClicked();
    public void Player2GiveUpClicked() => State.Player2GiveUpClicked();

    public void Player1PlayAgain() => State.Player1PlayAgain();
    public void Player2PlayAgain() => State.Player2PlayAgain();

    public (GameDto?, PlayersStatisticsChanges?) RefreshGame() => State.RefreshGame();

    public void ChangeState(ISessionState newState)
    {
        State = newState;
    }

    public void RemoveSession()
    {
        _sessionManager.RemoveSession(Player1.Id);
        // нужно сделать что то еще???
    }

    public ESessionState GetStateEnum() => State.GetStateEnum();

    public void PublishRequest(GameRequestDto request)
    {
        _sessionManager.PublishRequest(request);
    }

    public void OnGameStateChanged() => State.OnGameStateChanged();
}
