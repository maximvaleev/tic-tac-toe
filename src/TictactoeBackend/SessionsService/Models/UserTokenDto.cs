﻿namespace SessionsService.Models
{
    public class UserTokenDto
    {
        public int UserId { get; set; }
        public string Token { get; set; }

        public UserTokenDto(int userId, string token)
        {
            UserId = userId;
            Token = token;
        }
    }
}
