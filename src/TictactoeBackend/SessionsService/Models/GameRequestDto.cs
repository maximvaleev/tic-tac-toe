﻿using SessionsService.Enums;

namespace SessionsService.Models;

public class GameRequestDto
{

    public int GameId { get; set; }
    public int GameActionCode { get; set; }
    public int ButtonIndex { get; set; }

    public GameRequestDto(int gameId, EGameAction gameAction, int buttonIndex = -1)
    {
        GameId = gameId;
        GameActionCode = (int)gameAction;
        ButtonIndex = buttonIndex;
    }
}
