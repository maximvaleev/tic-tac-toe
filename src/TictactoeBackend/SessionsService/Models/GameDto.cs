﻿namespace SessionsService.Models;

public class GameDto
{
    public int GameId { get; set; }
    public int GameStateCode { get; set; }
    public bool Player1PlaysX { get; set; }
    public int TurnNumber { get; set; }
    public int[] GameBoard { get; set; } = null!;
}


