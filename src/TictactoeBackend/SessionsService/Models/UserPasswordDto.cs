﻿namespace SessionsService.Models;

public class UserPasswordDto
{
    public string UserName { get; set; } = null!;
    public string Password { get; set; } = null!;
}
