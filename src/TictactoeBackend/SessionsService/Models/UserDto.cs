﻿using SessionsDatabase.Models;

namespace SessionsService.Models;

public class UserDto
{
    public int Id { get; set; }
    public string UserName { get; set; }
    public int WinsCount { get; set; }
    public int LossesCount { get; set; }
    public int DrawsCount { get; set; }

    public UserDto(int id, string userName, int winsCount, int lossesCount, int drawsCount)
    {
        Id = id;
        UserName = userName;
        WinsCount = winsCount;
        LossesCount = lossesCount;
        DrawsCount = drawsCount;
    }

    public UserDto(User user)
    {
        Id = user.Id;
        UserName = user.UserName;
        WinsCount = user.WinsCount;
        LossesCount = user.LossesCount;
        DrawsCount = user.DrawsCount;
    }
}
