﻿namespace SessionsService.Models;

public class SessionIdNameDto
{
    public int Id { get; set; }
    public string Name { get; set; }

    public SessionIdNameDto(Session session)
    {
        Id = session.Player1.Id;
        Name = session.Player1.UserName;
    }
}
