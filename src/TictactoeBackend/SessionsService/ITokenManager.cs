﻿using SessionsDatabase.Models;

namespace SessionsService;

public interface ITokenManager
{
    string CreateToken(User user);
    User? GetUser(string token);
    bool IsTokenExists(string token);
    void RemoveToken(string? token);
    string ValidateAndCleanseToken(string userToken);
    void AddTestUser(string token, User? user);
}