﻿namespace SessionsService.Enums;

public enum EGameState
{
    None = 0,
    Player1Turn = 1,
    Player2Turn = 2,
    Player1Win = 3,
    Player2Win = 4,
    Draw = 5,
    Player1Left = 6,
    Player2Left = 7,
    Player1GiveUp = 8,
    Player2GiveUp = 9,
    Player1Timeout = 10,
    Player2Timeout = 11,
}
