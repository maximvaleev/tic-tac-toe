﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SessionsDatabase.Models;
using SessionsService.Models;
using SessionsService.Services.UsersService;

namespace SessionsService.Controllers;

[Route("api/[controller]")]
[ApiController]
public class UsersController : ControllerBase
{
    private readonly IUsersService _usersService;

    public UsersController(IUsersService usersService)
    {
        _usersService = usersService;
    }

    /// <summary> Логин/регистрация. Создать или получить пользователя </summary>
    /// <remarks> принимает имя пользователя и пароль в теле запроса, возвращает ID пользователя и его токен </remarks>
    /// <param name="userData"> имя пользователя и пароль из тела запроса</param>
    /// <param name="cancellationToken"> необязательный токен отмены</param>
    /// <returns> возвращает ID пользователя и его токен</returns>
    [HttpPost]
    [ProducesResponseType<UserTokenDto>(200)]
    [ProducesResponseType<string>(400)]
    public async Task<ActionResult<UserTokenDto>> GetUserTokenAsync([FromBody] UserPasswordDto userData, CancellationToken cancellationToken = default)
    {
        try
        {
            UserTokenDto result = await _usersService.GetUserTokenAsync(userData.UserName, userData.Password, cancellationToken);
            return Ok(result);
        }
        catch (ArgumentException ex)
        { 
            return BadRequest(ex.Message);
        }
        catch(Exception ex)
        {
            return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
        }
    }

    /// <summary> Получить данные о пользователе по Id </summary>
    /// <remarks> Зарегистрированный пользователь может получить данные о любом пользователе в системе по Id. Принимает ID пользователя в URL, возвращает json с Id, UserName, WinsCount, LossesCount, DrawsCount </remarks>
    /// <param name="id">Id запрашиваемого пользователя</param>
    /// <param name="userToken">токен пользователя</param>
    /// <param name="cancellationToken">необязательный токен отмены</param>
    /// <returns>возвращает json с Id, UserName, WinsCount, LossesCount, DrawsCount</returns>
    [HttpGet("{id}")]
    [ProducesResponseType<UserDto>(200)]
    [ProducesResponseType<string>(401)]
    [ProducesResponseType<string>(404)]
    public async Task<ActionResult<UserDto>> GetUserAsync(int id, [FromHeader(Name = "Authorization")] string userToken, CancellationToken cancellationToken = default)
    {
        try
        {
            UserDto result = await _usersService.GetUserAsync(id, userToken, cancellationToken);
            return Ok(result);
        }
        catch (UnauthorizedAccessException ex)
        {
            return Unauthorized(ex.Message);
        }
        catch (KeyNotFoundException ex)
        {
            return NotFound(ex.Message);
        }
        catch (Exception ex)
        {
            return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
        }
    }
}
