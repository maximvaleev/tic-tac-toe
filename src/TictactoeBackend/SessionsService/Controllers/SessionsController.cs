﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SessionsService.Models;
using SessionsService.Services.SessionsService;
using SessionsService.Services.UsersService;

namespace SessionsService.Controllers;

[Route("api/[controller]")]
[ApiController]
public class SessionsController : ControllerBase
{
    private readonly ISessionsServiceRep _sessionsService;

    public SessionsController(ISessionsServiceRep sessionsService)
    {
        _sessionsService = sessionsService;
    }

    // <response code = "200"> Так можено комментировать коды ответов </response>

    /// <summary> Создать сессию для текущего пользователя </summary>
    /// <remarks> 
    /// возвращается информация о сессии. Если сессия уже есть, то новая не создается. В информации есть состояние сессии,
    /// это перечисление: "Создана и ожидает второго игрока" = 0, "Игра в процессе" = 1, "Игра завершена, ожидание решения игроков о повторе игры" = 2, 
    /// "Сессия деактивирована и скоро будет удалена" = 3
    ///  </remarks>
    /// <param name="userToken"> токен в заголовке в формате "Authorization" : "Bearer 1a2b3c4d" </param>
    /// <returns> возвращается информация о сессии </returns>
    [HttpPost]
    [ProducesResponseType<SessionDto>(200)]
    [ProducesResponseType<string>(401)]
    public ActionResult<SessionDto> GetOrCreateSession([FromHeader(Name = "Authorization")] string userToken)
    {
        try
        {
            SessionDto result = _sessionsService.GetOrCreateSession(userToken);
            return Ok(result);
        }
        catch (UnauthorizedAccessException ex) { return Unauthorized(ex.Message); }
        catch (Exception ex) { return StatusCode(StatusCodes.Status500InternalServerError, ex.Message); }
    }

    /// <summary> Назначить текущего пользователя 2м игроком в указанную сессию </summary>
    /// <remarks> возвращается информация о сессии. В информации есть состояние сессии,
    /// это перечисление: "Создана и ожидает второго игрока" = 0, "Игра в процессе" = 1, "Игра завершена, ожидание решения игроков о повторе игры" = 2, 
    /// "Сессия деактивирована и скоро будет удалена" = 3
    /// </remarks>
    /// <param name="userToken"> токен в заголовке в формате "Authorization" : "Bearer 1a2b3c4d" </param>
    /// <param name="sessionId"> SessionId всегда совпадает с UserId 1го игрока </param>
    /// <returns> возвращается информация о сессии. </returns>
    [HttpPut("{sessionId}")]
    [ProducesResponseType<SessionDto>(200)]
    [ProducesResponseType<string>(401)]
    [ProducesResponseType<string>(404)]
    [ProducesResponseType<string>(400)]
    public ActionResult<SessionDto> AssignUserAsPlayer2ToSession([FromHeader(Name = "Authorization")] string userToken, int sessionId)
    {
        try
        {
            SessionDto sessionDto = _sessionsService.AssignUserAsPlayer2ToSession(userToken, sessionId);
            return Ok(sessionDto);
        }
        catch (UnauthorizedAccessException ex) { return Unauthorized(ex.Message); }
        catch (KeyNotFoundException ex) { return NotFound(ex.Message); }
        catch (ArgumentException ex) { return BadRequest(ex.Message); }
        catch (Exception ex) { return StatusCode(StatusCodes.Status500InternalServerError, ex.Message); }
    }

    /// <summary> Запрос на получение списка сессий в состоянии "Создана и ожидает второго игрока" </summary>
    /// <remarks> токен авторизации не требуется и будет проигнорирован. Возвращается коллекция элементов с Id и SessionName.
    /// Эти значения совпадают с UserId и UserName первого игрока.</remarks>
    /// <returns> коллекция сессий </returns>
    [HttpGet]
    [ProducesResponseType<IEnumerable<SessionIdNameDto>>(200)]
    public ActionResult<IEnumerable<SessionIdNameDto>> GetCreatedSessionsList()
    {
        try
        {
            IEnumerable<SessionIdNameDto> sessions = _sessionsService.GetCreatedSessionsList();
            return Ok(sessions);
        }
        catch (Exception ex) { return StatusCode(StatusCodes.Status500InternalServerError, ex.Message); }
    }

    /// <summary> Удалить текущего пользователся из указанной сессии </summary>
    /// <remarks> Текущий пользователь может быть как первым, так и вторым игроком. </remarks>
    /// <param name="userToken"> токен в заголовке в формате "Authorization" : "Bearer 1a2b3c4d" </param>
    /// <param name="sessionId"> SessionId всегда совпадает с UserId 1го игрока </param>
    [HttpDelete("{sessionId}")]
    [ProducesResponseType(200)]
    [ProducesResponseType<string>(401)]
    [ProducesResponseType<string>(404)]
    [ProducesResponseType<string>(400)]
    public ActionResult DeleteUserFromSession([FromHeader(Name = "Authorization")] string userToken, int sessionId)
    {
        try
        {
            _sessionsService.DeleteUserFromSession(userToken, sessionId);
            return Ok();
        }
        catch (UnauthorizedAccessException ex) { return Unauthorized(ex.Message); }
        catch (KeyNotFoundException ex) { return NotFound(ex.Message); }
        catch (ArgumentException ex) { return BadRequest(ex.Message); }
        catch (Exception ex) { return StatusCode(StatusCodes.Status500InternalServerError, ex.Message); }
    }

    /// <summary> Получить инфо о сесси по ID </summary>
    /// <remarks> возвращается информация о сессии. В информации есть состояние сессии,
    /// это перечисление: "Создана и ожидает второго игрока" = 0, "Игра в процессе" = 1, "Игра завершена, ожидание решения игроков о повторе игры" = 2, 
    /// "Сессия деактивирована и скоро будет удалена" = 3 </remarks>
    /// <param name="userToken"> токен в заголовке в формате "Authorization" : "Bearer 1a2b3c4d" </param>
    /// <param name="sessionId"> SessionId всегда совпадает с UserId 1го игрока </param>
    /// <returns> возвращается информация о сессии. </returns>
    [HttpGet("{sessionId}")]
    [ProducesResponseType<SessionDto>(200)]
    [ProducesResponseType<string>(401)]
    [ProducesResponseType<string>(404)]
    public ActionResult<SessionDto> GetSession([FromHeader(Name = "Authorization")] string userToken, int sessionId)
    {
        try
        {
            SessionDto sessionDto = _sessionsService.GetSession(userToken, sessionId);
            return Ok(sessionDto);
        }
        catch (UnauthorizedAccessException ex) { return Unauthorized(ex.Message); }
        catch (KeyNotFoundException ex) { return NotFound(ex.Message); }
        catch (Exception ex) { return StatusCode(StatusCodes.Status500InternalServerError, ex.Message); }
    }

    [HttpGet("{sessionId}/game")]
    [ProducesResponseType<GameDto>(200)]
    [ProducesResponseType<string>(401)]
    [ProducesResponseType<string>(404)]
    [ProducesResponseType<string>(400)]
    public async Task<ActionResult<GameDto?>> GetGameAsync([FromHeader(Name = "Authorization")] string userToken, int sessionId, CancellationToken cancellationToken)
    {
        try
        {
            GameDto? game = await _sessionsService.GetGameAsync(userToken, sessionId, cancellationToken);
            return Ok(game);
        }
        catch (UnauthorizedAccessException ex) { return Unauthorized(ex.Message); }
        catch (KeyNotFoundException ex) { return NotFound(ex.Message); }
        catch (ArgumentException ex) { return BadRequest(ex.Message); }
        catch (Exception ex) { return StatusCode(StatusCodes.Status500InternalServerError, ex.Message); }
    }

    [HttpPut("{sessionId}/game/giveup")]
    [ProducesResponseType(200)]
    [ProducesResponseType<string>(401)]
    [ProducesResponseType<string>(404)]
    [ProducesResponseType<string>(400)]
    public ActionResult GiveUpButtonClicked([FromHeader(Name = "Authorization")] string userToken, int sessionId)
    {
        try
        {
            _sessionsService.GiveUpButtonClicked(userToken, sessionId);
            return Ok();
        }
        catch (UnauthorizedAccessException ex) { return Unauthorized(ex.Message); }
        catch (KeyNotFoundException ex) { return NotFound(ex.Message); }
        catch (ArgumentException ex) { return BadRequest(ex.Message); }
        catch (Exception ex) { return StatusCode(StatusCodes.Status500InternalServerError, ex.Message); }
    }

    [HttpPut("{sessionId}/game")]
    [ProducesResponseType(200)]
    [ProducesResponseType<string>(401)]
    [ProducesResponseType<string>(404)]
    [ProducesResponseType<string>(400)]
    public ActionResult GameButtonClicked([FromHeader(Name = "Authorization")] string userToken, int sessionId, int cell)
    {
        try
        {
            _sessionsService.GameButtonClicked(userToken, sessionId, cell);
            return Ok();
        }
        catch (UnauthorizedAccessException ex) { return Unauthorized(ex.Message); }
        catch (KeyNotFoundException ex) { return NotFound(ex.Message); }
        catch (ArgumentException ex) { return BadRequest(ex.Message); }
        catch (Exception ex) { return StatusCode(StatusCodes.Status500InternalServerError, ex.Message); }
    }

    [HttpPut("{sessionId}/game/playagain")]
    [ProducesResponseType(200)]
    [ProducesResponseType<string>(401)]
    [ProducesResponseType<string>(404)]
    [ProducesResponseType<string>(400)]
    public ActionResult PlayAgainClicked([FromHeader(Name = "Authorization")] string userToken, int sessionId)
    {
        try
        {
            _sessionsService.PlayAgainClicked(userToken, sessionId);
            return Ok();
        }
        catch (UnauthorizedAccessException ex) { return Unauthorized(ex.Message); }
        catch (KeyNotFoundException ex) { return NotFound(ex.Message); }
        catch (ArgumentException ex) { return BadRequest(ex.Message); }
        catch (Exception ex) { return StatusCode(StatusCodes.Status500InternalServerError, ex.Message); }
    }
}
