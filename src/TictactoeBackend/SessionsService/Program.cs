
using GameService;
using Microsoft.EntityFrameworkCore;
using Microsoft.OpenApi.Models;
using RabbitMQ.Client.Events;
using SessionsDatabase.Contexts;
using SessionsService.Services.SessionsService;
using SessionsService.Services.UsersService;
using System.Reflection;
using System.Text;

namespace SessionsService;

public class Program
{
    public static void Main(string[] args)
    {
        var builder = WebApplication.CreateBuilder(args);

        // Add services to the container.

        builder.Services.AddControllers();
        // Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
        builder.Services.AddEndpointsApiExplorer();

        var info = new OpenApiInfo()
        {
            Title = "������ ������. API ������������",
            Version = "v1",
            Description = "������ ������ � ������ ������� \n\t\"��������-������\"",
            Contact = new OpenApiContact()
            {
                Name = "������ �.",
                Email = "valeevphoto@gmail.com",
            }
        };
        builder.Services.AddSwaggerGen(c =>
        {
            c.SwaggerDoc("v1", info);
            var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
            var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
            c.IncludeXmlComments(xmlPath);
        });

        builder.Services.AddSingleton<ITokenManager, TokenManager>();
        builder.Services.AddSingleton<ISessionManager, SessionManager>();
        builder.Services.AddScoped<IUsersService, UsersService>();
        builder.Services.AddScoped<ISessionsServiceRep, SessionsServiceRep>();

        string? connection = builder.Configuration.GetConnectionString("DefaultConnection");
        //if (string.IsNullOrWhiteSpace(connection)) throw new Exception("Connection string is empty");
        builder.Services.AddDbContext<UsersContext>(options => options.UseNpgsql(connection));

        // CORS
        builder.Services.AddCors(options =>
        {
            options.AddDefaultPolicy(policy =>
            {
                policy
                    .AllowAnyOrigin()
                    .AllowAnyHeader()
                    .AllowAnyMethod();
            });
        });

        var app = builder.Build();

        // Configure the HTTP request pipeline.
        if (app.Environment.IsDevelopment())
        {
            app.UseSwagger();
            app.UseSwaggerUI();
            app.ApplyMigrations();
        }

        app.UseHttpsRedirection();

        app.UseCors();
        
        app.UseAuthorization();

        app.MapControllers();


        app.Run();


    }
}
