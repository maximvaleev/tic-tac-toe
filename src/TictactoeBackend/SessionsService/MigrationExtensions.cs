﻿using Microsoft.EntityFrameworkCore;
using SessionsDatabase;
using SessionsDatabase.Contexts;

namespace SessionsService;

public static class MigrationExtensions
{
    public static void ApplyMigrations(this IApplicationBuilder app)
    {
        using IServiceScope scope = app.ApplicationServices.CreateScope();

        using UsersContext dbContext = scope.ServiceProvider.GetRequiredService<UsersContext>();

        dbContext.Database.Migrate();
    }
}
