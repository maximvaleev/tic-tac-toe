﻿using SessionsService.Models;

namespace SessionsService.Services.SessionsService
{
    public interface ISessionsServiceRep
    {
        SessionDto AssignUserAsPlayer2ToSession(string userToken, int sessionId);
        SessionDto GetOrCreateSession(string userToken);
        IEnumerable<SessionIdNameDto> GetCreatedSessionsList();
        void DeleteUserFromSession(string userToken, int sessionId);
        SessionDto GetSession(string userToken, int sessionId);
        Task<GameDto?> GetGameAsync(string userToken, int sessionId, CancellationToken cancellationToken);
        void GiveUpButtonClicked(string userToken, int sessionId);
        void GameButtonClicked(string userToken, int sessionId, int cellIndex);
        void PlayAgainClicked(string userToken, int sessionId);
    }
}
