﻿using SessionsDatabase.Contexts;
using SessionsDatabase.Models;
using SessionsService.Models;

namespace SessionsService.Services.SessionsService
{
    public class SessionsServiceRep : ISessionsServiceRep
    {
        private readonly UsersContext _context;
        private readonly ITokenManager _tokenManager;
        private readonly ISessionManager _sessionManager;

        public SessionsServiceRep(UsersContext context, ITokenManager tokenManager, ISessionManager sessionManager)
        {
            _context = context;
            _tokenManager = tokenManager;
            _sessionManager = sessionManager;
        }

        public SessionDto AssignUserAsPlayer2ToSession(string userToken, int sessionId)
        {
            // валидация токена. Если не проходит, то бросает UnauthorizedAccessException
            userToken = _tokenManager.ValidateAndCleanseToken(userToken);
            User user = _tokenManager.GetUser(userToken) ?? throw new Exception("Ошибка в программе. После валидации токена пользователя не оказалось.");
            Session session = _sessionManager.GetById(sessionId) ?? throw new KeyNotFoundException($"Сессии с указанным Id={sessionId} не существует.");
            session.AddPlayer2(user);
            return new SessionDto(session);
        }

        public SessionDto GetOrCreateSession(string userToken)
        {
            // валидация токена. Если не проходит, то бросает UnauthorizedAccessException
            userToken = _tokenManager.ValidateAndCleanseToken(userToken);

            User user = _tokenManager.GetUser(userToken) ?? throw new Exception("Ошибка в программе. После валидации токена пользователя не оказалось.");
            Session session = _sessionManager.GetOrCreate(user);
            return new SessionDto(session);
        }

        public IEnumerable<SessionIdNameDto> GetCreatedSessionsList() 
        {
            return _sessionManager.GetCreatedSessionsList();
        }

        public void DeleteUserFromSession(string userToken, int sessionId)
        {
            // валидация токена. Если не проходит, то бросает UnauthorizedAccessException
            userToken = _tokenManager.ValidateAndCleanseToken(userToken);
            User user = _tokenManager.GetUser(userToken) ?? throw new Exception("Ошибка в программе. После валидации токена пользователя не оказалось.");
            _sessionManager.DeleteUserFromSession(sessionId, user);
        }

        public SessionDto GetSession(string userToken, int sessionId)
        {
            _ = _tokenManager.ValidateAndCleanseToken(userToken);
            Session session = _sessionManager.GetById(sessionId) ?? throw new KeyNotFoundException($"Сессии с указанным Id={sessionId} не существует.");
            return new SessionDto(session);
        }

        public async Task<GameDto?> GetGameAsync(string userToken, int sessionId, CancellationToken cancellationToken)
        {
            _ = _tokenManager.ValidateAndCleanseToken(userToken);
            Session session = _sessionManager.GetById(sessionId) ?? throw new KeyNotFoundException($"Сессии с указанным Id={sessionId} не существует.");
            (GameDto? game, PlayersStatisticsChanges? changes) = session.RefreshGame();

            // сохранить изменения статистики в БД
            if (changes is not null)
            {
                // Player 1
                User? user1 = await _context.Users.FindAsync(new object[] { session.Player1.Id }, cancellationToken);
                if (user1 != null)
                {
                    user1.LossesCount += changes.Player1LossesCount;
                    user1.WinsCount += changes.Player1WinsCount;
                    user1.DrawsCount += changes.Player1DrawsCount;
                }
                else
                {
                    throw new Exception("Ошибка на сервере. Игрок 1 для записи статистики не найден.");
                }
                // Player 2
                User? user2 = await _context.Users.FindAsync(new object?[] { session.Player2?.Id }, cancellationToken);
                if (user2 != null)
                {
                    user2.LossesCount += changes.Player2LossesCount;
                    user2.WinsCount += changes.Player2WinsCount;
                    user2.DrawsCount += changes.Player2DrawsCount;
                }
                else
                {
                    throw new Exception("Ошибка на сервере. Игрок 2 для записи статистики не найден.");
                }
                await _context.SaveChangesAsync(cancellationToken);
            }

            return game;
        }

        public void GiveUpButtonClicked(string userToken, int sessionId)
        {
            userToken = _tokenManager.ValidateAndCleanseToken(userToken);
            User user = _tokenManager.GetUser(userToken) ?? throw new Exception("Ошибка в программе. После валидации токена пользователя не оказалось.");
            Session session = _sessionManager.GetById(sessionId) ?? throw new KeyNotFoundException($"Сессии с указанным Id={sessionId} не существует.");
            if (user.Id == session.Player1.Id)
            {
                session.Player1GiveUpClicked();
            }
            else if (session.Player2 is not null && user.Id == session.Player2.Id)
            {
                session.Player2GiveUpClicked();
            }
            else
            {
                throw new ArgumentException("Текущий пользователь не принадлежит к сессии.");
            }
        }

        public void GameButtonClicked(string userToken, int sessionId, int cellIndex)
        {
            // валидация индекса кнопки
            if (cellIndex < 0 || cellIndex > 8)
            {
                throw new ArgumentException("Неверный индекс кнопки. Должно быть от 0 до 8.");
            }
            
            userToken = _tokenManager.ValidateAndCleanseToken(userToken);
            User user = _tokenManager.GetUser(userToken) ?? throw new Exception("Ошибка в программе. После валидации токена пользователя не оказалось.");
            Session session = _sessionManager.GetById(sessionId) ?? throw new KeyNotFoundException($"Сессии с указанным Id={sessionId} не существует.");
            if (user.Id == session.Player1.Id)
            {
                session.Player1GameButtonClicked(cellIndex);
            }
            else if (session.Player2 is not null && user.Id == session.Player2.Id)
            {
                session.Player2GameButtonClicked(cellIndex);
            }
            else
            {
                throw new ArgumentException("Текущий пользователь не принадлежит к сессии.");
            }
        }

        public void PlayAgainClicked(string userToken, int sessionId)
        {
            userToken = _tokenManager.ValidateAndCleanseToken(userToken);
            User user = _tokenManager.GetUser(userToken) ?? throw new Exception("Ошибка в программе. После валидации токена пользователя не оказалось.");
            Session session = _sessionManager.GetById(sessionId) ?? throw new KeyNotFoundException($"Сессии с указанным Id={sessionId} не существует.");
            if (user.Id == session.Player1.Id)
            {
                session.Player1PlayAgain();
            }
            else if (session.Player2 is not null && user.Id == session.Player2.Id)
            {
                session.Player2PlayAgain();
            }
            else
            {
                throw new ArgumentException("Текущий пользователь не принадлежит к сессии.");
            }
        }
    }
}
