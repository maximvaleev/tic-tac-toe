﻿
using SessionsDatabase.Models;
using SessionsService.Models;

namespace SessionsService.Services.UsersService;

public interface IUsersService
{
    Task<UserTokenDto> GetUserTokenAsync(string userName, string password, CancellationToken cancellationToken);
    Task<UserDto> GetUserAsync(int id, string userToken, CancellationToken cancellationToken);
}
