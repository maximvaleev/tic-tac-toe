﻿using Microsoft.EntityFrameworkCore;
using SessionsDatabase.Contexts;
using SessionsDatabase.Models;
using SessionsService.Models;
using System;
using System.Runtime.InteropServices;
using System.Threading;

namespace SessionsService.Services.UsersService;

public class UsersService : IUsersService
{
    private readonly UsersContext _context;
    private readonly ITokenManager _tokenManager;

    public UsersService(UsersContext context, ITokenManager tokenManager)
    {
        _context = context;
        _tokenManager = tokenManager;
        // добавить тестового юзера без таймаута и с известным токеном
        // это name=test, pass=test, id=4
        User? user = _context.Users.Find(new object[] { 4 });
        _tokenManager.AddTestUser("12345678", user);
    }

    public async Task<UserTokenDto> GetUserTokenAsync(string userName, string password, CancellationToken cancellationToken)
    {
        // валидация логина и пароля
        if (string.IsNullOrWhiteSpace(userName) || string.IsNullOrWhiteSpace(password))
        {
            throw new ArgumentException("Логин или пароль не могут быть пустыми.");
        }
        if (userName.Length > 32 || password.Length > 32)
        {
            throw new ArgumentException("Слишком длинный логин или пароль.");
        }

        User? user = await _context.Users.FirstOrDefaultAsync(x => x.UserName == userName, cancellationToken);
        
        if (user == null)
        {
            // это новый пользователь. Зарегистрировать, создать запись
            _context.Users.Add(new User { UserName = userName, Password = password });
            await _context.SaveChangesAsync(cancellationToken);
            user = await _context.Users.FirstAsync(x => x.UserName == userName, cancellationToken);
        }
        else if (user.Password != password)
        {
            // логин есть, но пароль не подходит
            throw new ArgumentException("Пользователь с таким логином уже есть в системе, либо пароль неверный.");
        }
        // пароль подошел к существующему пользователю или был создан новый. Делаем токен
        string token = _tokenManager.CreateToken(user);
        UserTokenDto userToken = new(user.Id, token);
        return userToken;
    }

    public async Task<UserDto> GetUserAsync(int id, string userToken, CancellationToken cancellationToken)
    {
        // валидация токена. Если не проходит, то бросает UnauthorizedAccessException
        userToken = _tokenManager.ValidateAndCleanseToken(userToken);

        User? user = await _context.Users.FindAsync(new object[] { id }, cancellationToken);
        
        return user is not null ? new UserDto(user) : throw new KeyNotFoundException($"Пользователь с id = {id} не найден.");
    }


}
