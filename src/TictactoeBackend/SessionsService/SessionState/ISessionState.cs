﻿using SessionsDatabase.Models;
using SessionsService.Enums;
using SessionsService.Models;

namespace SessionsService.SessionState;

public interface ISessionState
{
    void AddPlayer2(User player2);
    void RemovePlayer2();
    void RemovePlayer1();
    void Player1GameButtonClicked(int buttonId);
    void Player2GameButtonClicked(int buttonId);
    void Player1GiveUpClicked();
    void Player2GiveUpClicked();
    void Player1PlayAgain();
    void Player2PlayAgain();
    (GameDto?, PlayersStatisticsChanges?) RefreshGame();
    ESessionState GetStateEnum();
    void OnGameStateChanged();
}
