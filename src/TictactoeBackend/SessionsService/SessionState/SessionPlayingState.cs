﻿using GameService;
using SessionsDatabase.Models;
using SessionsService.Enums;
using SessionsService.Models;

namespace SessionsService.SessionState;

public class SessionPlayingState : ISessionState
{
    private readonly Session _session;
    private readonly int _sessionId;

    public SessionPlayingState(Session session)
    {
        _session = session;
        _sessionId = _session.Player1.Id;
        // создать новую игру
        _session.PublishRequest(new GameRequestDto(_sessionId, EGameAction.CreateGame));
    }

    public void AddPlayer2(User player2) => throw new ArgumentException("Второй игрок уже есть");

    public void RemovePlayer1()
    {
        // отправить уведомление в игровой сервис
        _session.PublishRequest(new GameRequestDto(_sessionId, EGameAction.P1Left));
        // игрока из свойства не удалять
        // перейти в состояние ResultsState
        _session.ChangeState(new SessionResultsState(_session));
    }

    public void RemovePlayer2()
    {
        // отправить уведомление в игровой сервис
        _session.PublishRequest(new GameRequestDto(_sessionId, EGameAction.P2Left));
        // игрока из свойства не удалять
        // перейти в состояние ResultsState
        _session.ChangeState(new SessionResultsState(_session));
    }

    public void Player1GameButtonClicked(int buttonId)
    {
        _session.PublishRequest(new GameRequestDto(_sessionId, EGameAction.P1Button, buttonId));
    }

    public void Player2GameButtonClicked(int buttonId)
    {
        _session.PublishRequest(new GameRequestDto(_sessionId, EGameAction.P2Button, buttonId));
    }

    public void Player1GiveUpClicked()
    {
        _session.PublishRequest(new GameRequestDto(_sessionId, EGameAction.P1GiveUp));
    }

    public void Player2GiveUpClicked()
    {
        _session.PublishRequest(new GameRequestDto(_sessionId, EGameAction.P2GiveUp));
    }

    public void Player1PlayAgain() => throw new ArgumentException("Игра еще не завершена");
    public void Player2PlayAgain() => throw new ArgumentException("Игра еще не завершена");

    public (GameDto?, PlayersStatisticsChanges?) RefreshGame()
    {
        return (_session.GameDto, null);
    }

    public ESessionState GetStateEnum() => ESessionState.Playing;

    public void OnGameStateChanged()
    {
        EGameState state = _session.GameDto is not null ? (EGameState)_session.GameDto.GameStateCode : EGameState.None;

        switch (state)
        {
            case EGameState.Player1Win:
            case EGameState.Player2Win:
            case EGameState.Draw:
            case EGameState.Player1Left:
            case EGameState.Player2Left:
            case EGameState.Player1GiveUp:
            case EGameState.Player2GiveUp:
            case EGameState.Player1Timeout:
            case EGameState.Player2Timeout:
                // если игра завершилась, то переходим в состояние результатов
                _session.ChangeState(new SessionResultsState(_session));
                break;
            default:
                // в остальных случаях ничего не делаем
                break;
        }
    }
}
