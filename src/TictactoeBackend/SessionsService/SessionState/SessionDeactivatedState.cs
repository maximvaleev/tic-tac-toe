﻿using SessionsDatabase.Models;
using SessionsService.Enums;
using SessionsService.Models;

namespace SessionsService.SessionState;

public class SessionDeactivatedState : ISessionState
{
    private const int _deletionTime = 60_000;
    private readonly Session _session;
    private readonly Timer _deletionTimer;

    public SessionDeactivatedState(Session session)
    {
        _session = session;
        _deletionTimer = new(DeletionTimeout, null, _deletionTime, Timeout.Infinite);
    }

    private void DeletionTimeout(object? _)
    {
        // удалить таймер 
        _deletionTimer.Dispose();
        // удалить игру
        _session.PublishRequest(new GameRequestDto(_session.Player1.Id, EGameAction.DeleteGame));
        // удалить сессию из списка активных (т.е. фактически удалить ее совсем)
        _session.RemoveSession();
    }

    public void AddPlayer2(User player2) => throw new ArgumentException("Сессия деактивирована");

    public void RemovePlayer1() => throw new ArgumentException("Сессия деактивирована");
    public void RemovePlayer2() => throw new ArgumentException("Сессия деактивирована");

    public void Player1GameButtonClicked(int buttonId) => throw new ArgumentException("Сессия деактивирована");
    public void Player2GameButtonClicked(int buttonId) => throw new ArgumentException("Сессия деактивирована");

    public void Player1GiveUpClicked() => throw new ArgumentException("Сессия деактивирована");
    public void Player2GiveUpClicked() => throw new ArgumentException("Сессия деактивирована");

    public void Player1PlayAgain() => throw new ArgumentException("Сессия деактивирована");
    public void Player2PlayAgain() => throw new ArgumentException("Сессия деактивирована");

    public (GameDto?, PlayersStatisticsChanges?) RefreshGame()
    {
        _deletionTimer.Change(_deletionTime, Timeout.Infinite);
        return (_session.GameDto, null);
    }

    public ESessionState GetStateEnum() => ESessionState.Deactivated;

    public void OnGameStateChanged() { } // ничего не происходит, мы сюда по идее и не должны попасть

}
