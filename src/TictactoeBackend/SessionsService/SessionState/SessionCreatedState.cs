﻿using NuGet.Common;
using SessionsDatabase.Models;
using SessionsService.Enums;
using SessionsService.Models;

namespace SessionsService.SessionState
{
    public class SessionCreatedState : ISessionState
    {
        private const int _awaitPlayer2Time = 300_000;
        private readonly Session _session;
        private readonly Timer _awaitTimer;

        public SessionCreatedState(Session session)
        {
            _session = session;
            _awaitTimer = new(AwaitForPlayer2Timeout, null, _awaitPlayer2Time, Timeout.Infinite);
        }

        private void AwaitForPlayer2Timeout(object? _)
        {
            // удалить таймер ожидания
            _awaitTimer.Dispose();
            // перейти в DeactivatedState
            _session.ChangeState(new SessionDeactivatedState(_session));
        }

        public void AddPlayer2(User player2)
        {
            // проверим игрока
            if (player2.Id == _session.Player1.Id) 
            {
                throw new ArgumentException("Второй игрок совпадает с первым.");
            }
            // удалить таймер ожидания
            _awaitTimer.Dispose();
            // добавить игрока в свойство
            _session.Player2 = player2;
            // перейти в PlayingState
            _session.ChangeState(new SessionPlayingState(_session));
        }

        public void Player1GameButtonClicked(int buttonId) => throw new ArgumentException("Игра еще не начата");
        public void Player2GameButtonClicked(int buttonId) => throw new ArgumentException("Игра еще не начата");

        public void Player1GiveUpClicked() => throw new ArgumentException("Игра еще не начата");
        public void Player2GiveUpClicked() => throw new ArgumentException("Игра еще не начата");

        public void Player1PlayAgain() => throw new ArgumentException("Игра еще не сыграна");
        public void Player2PlayAgain() => throw new ArgumentException("Игра еще не сыграна");

        public void RemovePlayer1()
        {
            // удалить таймер ожидания
            _awaitTimer.Dispose();
            // перейти в DeactivatedState (игрока не удалять)
            _session.ChangeState(new SessionDeactivatedState(_session));
        }

        public void RemovePlayer2() => throw new ArgumentException("Нет второго игрока");

        public (GameDto?, PlayersStatisticsChanges?) RefreshGame() => throw new ArgumentException("Игра еще не начата");

        public ESessionState GetStateEnum() => ESessionState.Created;

        public void OnGameStateChanged() { } // ничего не происходит, мы сюда и не попадем
    }
}
