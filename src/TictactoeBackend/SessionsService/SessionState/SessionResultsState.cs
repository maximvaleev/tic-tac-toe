﻿using SessionsDatabase.Contexts;
using SessionsDatabase.Models;
using SessionsService.Enums;
using SessionsService.Models;
using SessionsService.Services.UsersService;

namespace SessionsService.SessionState;

public class SessionResultsState : ISessionState
{
    private const int _decisionTime = 15_000;
    private readonly Session _session;
    private readonly Timer _decisionTimer;
    private bool _player1WantsToPlay = false;
    private bool _player2WantsToPlay = false;
    private bool _isResultsSaved = false;
    private PlayersStatisticsChanges? _playersStatisticsChanges;

    public SessionResultsState(Session session)
    {
        _session = session;
        _decisionTimer = new(DecisionTimeout, null, _decisionTime, Timeout.Infinite);
        CheckStatusChanged();
    }

    // статус игры мог не успеть обновиться при создании состояния, а мог успеть. Проверяем.
    private void CheckStatusChanged()
    {
        if (_isResultsSaved || _playersStatisticsChanges is not null)
        {
            return;
        }
        
        // проверить успел ли обновиться статус игры
        EGameState gameState = _session.GameDto is not null ? (EGameState)_session.GameDto.GameStateCode : EGameState.None;
        if (gameState == EGameState.Player1Win || gameState == EGameState.Player2GiveUp || gameState == EGameState.Player2Timeout || gameState == EGameState.Player2Left)
        {
            // первый игрок победил
            PlayersStatisticsChanges changes = new();
            changes.Player1WinsCount = 1;
            changes.Player2LossesCount = 1;
            _playersStatisticsChanges = changes;
        }
        else if(gameState == EGameState.Player2Win || gameState == EGameState.Player1GiveUp || gameState == EGameState.Player1Timeout || gameState == EGameState.Player1Left)
        {
            // второй игрок победил
            PlayersStatisticsChanges changes = new();
            changes.Player2WinsCount = 1;
            changes.Player1LossesCount = 1;
            _playersStatisticsChanges = changes;
        }
        else if(gameState == EGameState.Draw)
        {
            // ничья
            PlayersStatisticsChanges changes = new();
            changes.Player1DrawsCount = 1;
            changes.Player2DrawsCount = 1;
            _playersStatisticsChanges = changes;
        }
        else
        {
            // в остальных случаях статус не успел обновиться, ничего не делать
        }

        // если автоматически перейти в Deactivated, то RefreshGame не успеет вызваться и статистика не будет записана в БД!

        // проверка условий. Возможно сразу же переход в Deactivated
        //if (gameState == EGameState.Player1Left || gameState == EGameState.Player2Left)
        //{
        //    _decisionTimer?.Dispose();
        //    _session.ChangeState(new SessionDeactivatedState(_session));
        //}
    }

    private void DecisionTimeout(object? _)
    {
        // удалить таймер принятия решения
        _decisionTimer.Dispose();
        // перейти в DeactivatedState
        _session.ChangeState(new SessionDeactivatedState(_session));
    }

    public void AddPlayer2(User player2) => throw new ArgumentException("В эту сессию уже нельзя добавить игрока");

    public void RemovePlayer2()
    {
        // удалить таймер принятия решения
        _decisionTimer.Dispose();
        // удалить второго игрока
        _session.Player2 = null;
        // перейти в DeactivatedState
        _session.ChangeState(new SessionDeactivatedState(_session));
    }

    public void RemovePlayer1()
    {
        // удалить таймер принятия решения
        _decisionTimer.Dispose();
        // перейти в DeactivatedState
        _session.ChangeState(new SessionDeactivatedState(_session));
    }

    public void Player1GameButtonClicked(int buttonId) => throw new ArgumentException("Игра уже завершена");

    public void Player2GameButtonClicked(int buttonId) => throw new ArgumentException("Игра уже завершена");

    public void Player1GiveUpClicked() => throw new ArgumentException("Игра уже завершена");

    public void Player2GiveUpClicked() => throw new ArgumentException("Игра уже завершена");

    public void Player1PlayAgain()
    {
        _player1WantsToPlay = true;
        if (_player2WantsToPlay) 
        {
            // удалить таймер принятия решения
            _decisionTimer.Dispose();
            // перейти в PlayingState
            _session.ChangeState(new SessionPlayingState(_session));
        }
        else
        {
            // ничего не делаем, ждем
        }
    }

    public void Player2PlayAgain()
    {
        _player2WantsToPlay = true;
        if (_player1WantsToPlay)
        {
            // удалить таймер принятия решения
            _decisionTimer.Dispose();
            // перейти в PlayingState
            _session.ChangeState(new SessionPlayingState(_session));
        }
        else
        {
            // ничего не делаем, ждем
        }
    }

    public (GameDto?, PlayersStatisticsChanges?) RefreshGame()
    {
        if (_playersStatisticsChanges is null || _isResultsSaved) 
        {
            return (_session.GameDto, null);
        }
        else
        {
            _isResultsSaved = true;
            return (_session.GameDto, _playersStatisticsChanges);
        }
    }

    public ESessionState GetStateEnum() => ESessionState.Results;

    public void OnGameStateChanged()
    {
        CheckStatusChanged();
    }
}
